#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define NMAX 100

typedef struct
{
    char elem[NMAX];
    int top;
} stack;

void init(stack *stk)
{
    stk->top = 0;
}

void push(stack *stk, char c)
{
    if (stk->top < NMAX)
    {
        stk->elem[stk->top] = c;
        stk->top++;
    }
}

char pop(stack *stk)
{
    char elem;
    if ((stk->top) > 0)
    {
        stk->top--;
        elem = stk->elem[stk->top];
        return elem;
    }
    else
        return 0;
}

char stk_top(stack *stk)
{
    if ((stk->top) > 0)
        return stk->elem[stk->top-1];
    else
        return 0;
}

char get_top(stack *stk)
{
    return stk->top;
}

int is_empty(stack *stk)
{
    if ((stk->top) == 0)
        return 1;
    else
        return 0;
}

int prior(char ch)
{
    if (ch == '*' || ch == '/')
        return 2;
    else if (ch == '+' || ch == '-')
        return 1;
    else if (ch == '(' || ch == ')')
        return 0;
    else
        return -1;
}

char *str_to_rpn(char *str)
{
    int k = 0;
    char *after = calloc(strlen(str), sizeof(char));
    stack *stk;
    stk = malloc(sizeof(stack));
    init(stk);

    for (int i = 0; i < strlen(str); ++i)
    {
        if (str[i] >= 'A' && str[i] <= 'Z')
        {
            after[k] = str[i];
            ++k;
        }
        else if (str[i] == '*' || str[i] == '/' || str[i] == '+' || str[i] == '-')
        {
            if (is_empty(stk) || (prior(stk_top(stk)) < prior(str[i])))
                push(stk, str[i]);
            else
            {
                while (prior(stk_top(stk)) >= prior(str[i]))
                {
                    after[k] = pop(stk);
                    ++k;
                }
                push(stk, str[i]);
            }
        }
        else if (str[i] == '(')
            push(stk, str[i]);
        else if (str[i] == ')')
        {
            while (stk_top(stk) != '(')
            {
                after[k] = pop(stk);
                ++k;
            }
            pop(stk);
        }
    }

    while (!is_empty(stk))
    {
        after[k] = pop(stk);
        ++k;
    }

    return after;
}

int main(int argc, char *argv[])
{
    int cell, jcell, var_offset, lines = 0, num = 0, offset = 0, var[128] = {0}, operand;
    char command[50], ch = '\0', *expr;
    char *input_filename = (argc > 1) ? argv[1] : NULL, *output_filename;
    FILE *input, *output;

    if ((input = fopen(input_filename, "r")) == NULL)
    {
        printf("ERROR file %s: not found\n", input_filename);
        return -1;
    }
    output_filename = strtok(input_filename, ".");
    sprintf(output_filename, "%s.tmp", output_filename);
    output = fopen(output_filename, "w+");

    while (!feof(input))
    {
        ch = getc(input);
        if (ch == '\n')
            ++lines;
    }
    rewind(input);
    ch = '\0';

    char **rows = calloc(lines, sizeof(char*));
    for (int i = 0; i < lines; ++i)
    {
        while (ch != '\n')
        {
            ch = getc(input);
            ++num;
        }
        fseek(input, -num, SEEK_CUR);
        rows[i] = calloc(num, sizeof(char));
        fgets(rows[i], 200, input);
        fseek(input, num, SEEK_CUR);
    }
    fclose(input);

    for (int i = 0; i < lines; ++i)
        for (int j = 0; j < 200; ++j)
            if (rows[i][j] == '\n')
            {
                rows[i][j] = '\0';
                continue;
            }

    var_offset = lines + 1;

    for (int i = 0; i < lines; ++i)
    {
        sscanf(rows[i], "%d %s", &cell, command);
        cell /= 10;
        if (strcmp(command, "REM") == 0)
            printf("");
        else if (strcmp(command, "INPUT") == 0)
        {
            sscanf(rows[i], "%*d %*s %c", &ch);
            if (var[(int)ch] == 0)
            {
                var[(int)ch] = var_offset;
                ++var_offset;
            }
            fprintf(output, "%02d READ %02d\n", cell + offset, var[(int)ch]);
        }
        else if (strcmp(command, "PRINT") == 0)
        {
            sscanf(rows[i], "%*d %*s %c", &ch);
            fprintf(output, "%02d WRITE %02d\n", cell + offset, var[(int)ch]);
        }
        else if (strcmp(command, "LET") == 0)
        {
            sscanf(rows[i], "%*d %*s %c", &ch);
            if (var[(int)ch] == 0)
            {
                var[(int)ch] = var_offset;
                ++var_offset;
            }
            expr = calloc(strlen(strchr(rows[i], '=') + 2), sizeof(char));
            expr = strchr(rows[i], '=') + 2;
            expr = str_to_rpn(expr);

            fprintf(output, "%02d LOAD %02d\n", cell + offset, var[(int)expr[0]]);
            ++offset;
            for (int k = 1; k < strlen(expr); ++k)
            {
                if (expr[k] == '+')
                {
                    fprintf(output, "%02d ADD %02d\n", cell + offset, var[(int)expr[k-1]]);
                    ++offset;
                }
                else if (expr[k] == '-')
                {
                    fprintf(output, "%02d SUB %02d\n", cell + offset, var[(int)expr[k-1]]);
                    ++offset;
                }
                else if (expr[k] == '*')
                {
                    fprintf(output, "%02d MUL %02d\n", cell + offset, var[(int)expr[k-1]]);
                    ++offset;
                }
                else if (expr[k] == '/')
                {
                    fprintf(output, "%02d DIVIDE %02d\n", cell + offset, var[(int)expr[k-1]]);
                    ++offset;
                }
            }
            fprintf(output, "%02d STORE %02d\n", cell + offset, var[(int)ch]);
            ++offset;
        }
        else if (strcmp(command, "IF") == 0)
        {
            if (strstr(rows[i], "= 0") != NULL || strstr(rows[i], "< 0") != NULL)
            {
                sscanf(rows[i], "%*d %*s %c", &ch);
                if (var[(int)ch] == 0)
                {
                    fprintf(output, "%02d LOAD %02d\n", cell + offset, var[(int)ch]);
                    ++offset;
                }
                if (strstr(rows[i], "GOTO") != NULL)
                {
                    sscanf(rows[i], "%*d %*s %*c %*c 0 %*s %d", &jcell);
                    if (strstr(rows[i], "= 0") != NULL)
                    {
                        fprintf(output, "%02d JZ %02d\n", cell + offset, jcell/10);
                        ++offset;
                    }
                    else if (strstr(rows[i], "< 0") != NULL)
                    {
                        fprintf(output, "%02d JNEG %02d\n", cell + offset, jcell/10);
                        ++offset;
                    }
                }
                else if (strstr(rows[i], "LET") != NULL)
                {
                    sscanf(rows[i], "%*d %*s %*c = 0 %*s %c", &ch);
                    expr = calloc(strlen(strchr(rows[i], '=') + 2), sizeof(char));
                    expr = strchr(rows[i], '=') + 2;
                    if (var[(int)ch] == 0)
                    {
                        var[(int)ch] = var_offset;
                        ++var_offset;
                    }
                    fprintf(output, "%02d LOAD %02d\n", cell + offset, var[(int)ch]);
                    ++offset;
                    if (strstr(rows[i], "= 0") != NULL)
                    {
                        expr = str_to_rpn(expr+10);
                        fprintf(output, "%02d JZ %02d\n", cell + offset, cell + offset + 2);
                        ++offset;
                    }
                    else if (strstr(rows[i], "< 0") != NULL)
                    {
                        expr = str_to_rpn(expr);
                        fprintf(output, "%02d JNEG %02d\n", cell + offset, cell + offset + 2);
                        ++offset;
                    }
                    fprintf(output, "%02d JUMP %02d\n", cell + offset, cell + offset*4 + 1);
                    ++offset;

                    fprintf(output, "%02d LOAD %02d\n", cell + offset, var[(int)expr[0]]);
                    ++offset;
                    for (int k = 1; k < strlen(expr); ++k)
                    {
                        if (expr[k] == '+')
                        {
                            fprintf(output, "%02d ADD %02d\n", cell + offset, var[(int)expr[k-1]]);
                            ++offset;
                        }
                        else if (expr[k] == '-')
                        {
                            fprintf(output, "%02d SUB %02d\n", cell + offset, var[(int)expr[k-1]]);
                            ++offset;
                        }
                        else if (expr[k] == '*')
                        {
                            fprintf(output, "%02d MUL %02d\n", cell + offset, var[(int)expr[k-1]]);
                            ++offset;
                        }
                        else if (expr[k] == '/')
                        {
                            fprintf(output, "%02d DIVIDE %02d\n", cell + offset, var[(int)expr[k-1]]);
                            ++offset;
                        }
                    }
                    fprintf(output, "%02d STORE %02d\n", cell + offset, var[(int)ch]);
                    ++offset;
                    fprintf(output, "%02d JUMP %02d\n", cell + offset, cell + offset*2);
                    offset *= 2;
                }
            }
        }
        else if (strcmp(command, "GOTO") == 0)
        {
            sscanf(rows[i], "%*d %*s %d", &jcell);
            fprintf(output, "%02d JUMP %02d\n", cell + offset, jcell/10);
        }
        else if (strcmp(command, "END") == 0)
            fprintf(output, "%02d HALT 00\n", cell + offset);
        else
        {
            printf("ERROR line %d: unknown command\n", i+1);
        }
    }

    char *tmp_filename;
    FILE *tmp;

    tmp_filename = strtok(input_filename, ".");
    sprintf(tmp_filename, "%s.sa", tmp_filename);

    if ((tmp = fopen(tmp_filename, "w+")) == NULL)
    {
        printf("ERROR file %s: can't create\n", tmp_filename);
        return -1;
    }

    rewind(output);
    lines = 0;
    ch = '\0';
    while (!feof(output))
    {
        ch = getc(output);
        if (ch == '\n')
            ++lines;
    }
    rewind(output);
    ch = '\0';

    for (int i = 0; i < lines; ++i)
    {
        fscanf(output, "%d %s %d", &cell, command, &operand);
        if (strcmp(command, "JNEG") != 0 && strcmp(command, "JUMP") != 0
        && strcmp(command, "JZ") != 0 && strcmp(command, "HALT") != 0)
            operand += offset;
        fprintf(tmp, "%02d %s %02d\n", cell, command, operand);
    }
    fclose(tmp);
    fclose(output);

    tmp_filename = strtok(tmp_filename, ".");
    sprintf(tmp_filename, "%s.tmp", tmp_filename);
    remove(tmp_filename);

    return 0;
}