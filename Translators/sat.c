#include <stdio.h>
#include <string.h>

int main(int argc, char *argv[])
{
    int line = 1, memory[100], cell, operand;
    char *filename = (argc > 1) ? argv[1] : NULL;
    char command[20], comment[100];
    FILE *input, *output;

    for (int i = 0; i < 100; ++i)
        memory[i] = 0;
    
    if ((input = fopen(filename, "r")) == NULL)
    {
        printf("ERROR file %s: not found\n", filename);
        return -1;
    }
    
    while (!feof(input))
    {
        fscanf(input, "%d %s %d ;%s\n", &cell, command, &operand, comment);

        if (cell < 0 || cell > 99)
        {
            printf("ERROR line %d: cell is out of memory array\n", line);
            return -2;
        }

        if (strcmp(command, "READ") == 0)
            memory[cell] = (10 << 7) | operand;
        else if (strcmp(command, "WRITE") == 0)
            memory[cell] = (11 << 7) | operand;

        else if (strcmp(command, "LOAD") == 0)
            memory[cell] = (20 << 7) | operand;
        else if (strcmp(command, "STORE") == 0)
            memory[cell] = (21 << 7) | operand;

        else if (strcmp(command, "ADD") == 0)
            memory[cell] = (30 << 7) | operand;
        else if (strcmp(command, "SUB") == 0)
            memory[cell] = (31 << 7) | operand;
        else if (strcmp(command, "DIVIDE") == 0)
            memory[cell] = (32 << 7) | operand;
        else if (strcmp(command, "MUL") == 0)
            memory[cell] = (33 << 7) | operand;

        else if (strcmp(command, "JUMP") == 0)
            memory[cell] = (40 << 7) | operand;
        else if (strcmp(command, "JNEG") == 0)
            memory[cell] = (41 << 7) | operand;
        else if (strcmp(command, "JZ") == 0)
            memory[cell] = (42 << 7) | operand;
        else if (strcmp(command, "HALT") == 0)
            memory[cell] = (43 << 7) | operand;

        else if (strcmp(command, "=") == 0)
            memory[cell] = operand;
        else
        {
            printf("ERROR line %d: unknown command\n", line);
            return -3;
        }

        if ((strcmp(command, "=") != 0) && (operand < 0 || operand > 99))
        {
            printf("ERROR line %d: operand is out of memory array\n", line);
            return -4;
        }

        ++line;
    }
    fclose(input);
    
    filename = strtok(filename, ".");
    sprintf(filename, "%s.scs", filename);

    output = fopen(filename, "wb");
    fwrite(memory, sizeof(*memory) * 100, 1, output);
    fclose(output);
    
    return 0;
}