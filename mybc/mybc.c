#include "../myterm/myterm.h"
#include "mybc.h"

int bc_printA(char* str)
{
    printf("\E(0%s\E(B", str);

    return 0;
}

int bc_box(int x1, int y1, int x2, int y2)
{
    printf("\E(0");

    mt_gotoXY(x1, y1, "");

    for (int i = 0; i < y2-y1; ++i)
    {
        for (int j = 0; j < x2-x1; ++j)
        {
            if (i == 0 && j == 0)
                printf("l");
            if (i == y2-y1-1 && j == 0)
                mt_gotoXY(x1, y1+i, "m");
            if ((i == 0 || i == y2-y1-1) && (j < x2-x1))
                printf("q");
            if (i > 0 && i < y2-y1-1 && j == 0)
                mt_gotoXY(x1, y1+i, "x");
            if (i > 0 && i < y2-y1-1)
                printf(" ");
            if (i > 0 && i < y2-y1-1 && j == x2-x1-1)
                printf("x");
            if (i == 0 && j == x2-x1-1)
                printf("k");
            if (i == y2-y1-1 && j == x2-x1-1)
                printf("j");
        }
        printf("\n");
    }

    printf("\E(B");

    return 0;
}

int bc_printbigchar(int* big, int x, int y, colors fg, colors bg)
{
    int i, j, pos, bit;
	char row[9];

	row[8] = '\0';
	mt_setfgcolor(fg);
	mt_setbgcolor(bg);

	for (i = 0; i < 8; i++)
    {
		for (j = 0; j < 8; j++)
		{
			pos = i >> 2;
			bit = (big[pos] >> ((i % 4) * 8 + j)) & 1;
			if (bit == 0)
				row[j] = ' ';
			else
				row[j] = 'a';
		}
		mt_gotoXY(x, y + i, "");
		bc_printA(row);
    }

	mt_setfgcolor(Default);
	mt_setbgcolor(Default);
    return 0;
}

int bc_setbigcharpos(int* big, int x, int y, int value)
{
	int pos;

	if ((x < 0) || (y < 0) || (x > 7) || (y > 7) || (value < 0) || (value > 1))
		return -1;
	if (y <= 3)
		pos = 0;
	else
		pos = 1;
	y = y % 4;
	if (value == 0)
		big[pos] &= ~(1 << (y*8 + x));
	else
		big[pos] |= 1 << (y*8 + x);

    return 0;
}

int bc_getbigcharpos(int* big, int x, int y, int* value)
{
	int pos;

	if ((x < 0) || (y < 0) || (x > 7) || (y > 7))
		return -1;
	if (y <= 3)
		pos = 0;
	else
		pos = 1;
	y = y % 4;
	*value = (big[pos] >> (y*8 + x)) & 1;

    return 0;
}

int bc_bigcharwrite(int fd, int* big, int count)
{
	int err;

	err = write(fd, &count, sizeof(count));
	if (err == -1)
		return -1;
	err = write(fd, big, count * (sizeof(int)) * 2);
	if (err == -1)
		return -1;

    return 0;
}

int bc_bigcharread(int fd, int* big, int need_count, int* count)
{
	int n, cnt, err;

	err = read(fd, &n, sizeof(n));
	if (err == -1 || (err != sizeof(n)))
		return -1;
	cnt = read(fd, big, need_count * sizeof(int) * 2);
	if (cnt == -1)
		return -1;
	*count = cnt / (sizeof(int) * 2);

    return 0;
}
