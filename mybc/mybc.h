#include <unistd.h>

#ifndef MYBC_H
#define MYBC_H

int bc_printA(char*);
int bc_box(int, int, int, int);
int bc_printbigchar(int*, int, int, colors, colors);
int bc_setbigcharpos(int*, int, int, int);
int bc_getbigcharpos(int*, int, int, int*);
int bc_bigcharwrite(int, int*, int);
int bc_bigcharread(int, int*, int, int*);

#endif // MYBC_H
