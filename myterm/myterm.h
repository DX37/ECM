#include <stdio.h>
#include <sys/ioctl.h>

#ifndef MYTERM_H
#define MYTERM_H

typedef enum {black, red, green, yellow, blue, magenta, cyan, light_gray, Default = 9} colors;

struct winsize ws;

int mt_clrscr();
int mt_gotoXY(int, int, char*);
int mt_getscreensize(int*, int*);
int mt_setfgcolor(colors);
int mt_setbgcolor(colors);

#endif // MYTERM_H
