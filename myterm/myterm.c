#include "myterm.h"

int mt_clrscr()
{
    printf("\E[H\E[J");

    return 0;
}

int mt_gotoXY(int x, int y, char* str)
{
    printf("\E[%d;%dH%s", y, x, str);

    return 0;
}

int mt_getscreensize(int* rows, int* cols)
{
    ioctl(1, TIOCGWINSZ, &ws);
    *rows = ws.ws_row;
    *cols = ws.ws_col;

    return 0;
}

int mt_setfgcolor(colors c)
{
    printf("\E[3%dm", c);

    return 0;
}

int mt_setbgcolor(colors c)
{
    printf("\E[4%dm", c);

    return 0;
}
