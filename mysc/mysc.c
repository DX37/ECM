#include "mysc.h"

int ops[] = {
    10, 11,
    20, 21,
    30, 31, 32, 33,
    40, 41, 42, 43,
    //51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76
};
int ops_amount = 38;

int sc_memoryInit()
{
    for (int i = 0; i < 100; ++i)
        memory[i] = 0;
    return 0;
}

int sc_memorySet(int address, int value)
{
    if ((address >= 0) && (address < 100))
    {
        memory[address] = value;
        return 0;
    }
    else
    {
        sc_regSet(FLAG_OUT_MEM, 1);
        return ERR_WRONG_ADDR;
    }
}

int sc_memoryGet(int address, int* value)
{
    if ((address >= 0) && (address < 100))
    {
        *value = memory[address];
        return 0;
    }
    else
    {
        sc_regSet(FLAG_OUT_MEM, 1);
        return ERR_WRONG_ADDR;
    }
}

int sc_memorySave(char* filename)
{
    FILE *dump;
    int nwritten;

    if ((dump = fopen(filename, "wb")) == NULL)
        return ERR_OPEN_FILE;

    nwritten = fwrite(memory, sizeof(*memory) * 100, 1, dump);
    fclose(dump);

    if (nwritten != 1)
        return ERR_FILE_SIZE;
    else
        return 0;
}

int sc_memoryLoad(char* filename)
{
    FILE *dump;
    int nreaden;

    if ((dump = fopen(filename, "rb")) == NULL)
        return ERR_OPEN_FILE;

    nreaden = fread(memory, sizeof(*memory) * 100, 1, dump);
    for (int i = 0; i < 100; ++i)
		memory[i] &= 0x7FFF;
    fclose(dump);

    if (nreaden != 1)
        return ERR_FILE_SIZE;
    else
        return 0;
}

int sc_regInit()
{
    reg_flags = 0;
    sc_regSet(FLAG_INTER_IGNORE, 1);
    return 0;
}

int sc_regSet(int reg, int value)
{
    if ((reg >= 1) && (reg <= 5))
    {
        if (value == 1)
            bit_set(reg_flags, reg);
        else if (value == 0)
            bit_clear(reg_flags, reg);
        else
            return ERR_WRONG_FLAG;
    }
    else
        return ERR_WRONG_FLAG;

    return 0;
}

int sc_regGet(int reg, int* value)
{
    if ((reg >= 1) && (reg <= 5))
        *value = bit_get(reg_flags, reg);
    else
        return ERR_WRONG_FLAG;

    return 0;
}

int sc_commandEncode(int command, int operand, int* value)
{
    for (int i = 0; i < ops_amount; ++i)
        if (command == ops[i])
        {
            *value = (command << 7) | operand;
            return 0;
        }

    return ERR_UNCORRECT_COMMAND;
}

int sc_commandDecode(int value, int* command, int* operand)
{
    if ((value >= 0) && (value <= 32767) && (bit_get(value, 14) == 0))
    {
        *command = (value >> 7) & 0x3F;
        *operand = value & 0x3F;
        for (int i = 0; i < ops_amount; ++i)
        	if (*command == ops[i])
        		return 0;
    }

	return ERR_UNCORRECT_COMMAND;
}
