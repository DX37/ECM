#include <stdio.h>

#ifndef MYSC_H
#define MYSC_H

#define bit_get(reg, val) (((reg) >> val) & 1)
#define bit_set(reg, val) (reg =((reg) | (1 << (val))))
#define bit_clear(reg, val) (reg = ((reg) & ~(1 << (val))))

#define FLAG_OVERFLOW 1
#define FLAG_ZERO_DIV 2
#define FLAG_OUT_MEM 3
#define FLAG_INTER_IGNORE 4
#define FLAG_WRONG_COM 5

#define ERR_WRONG_ADDR -1
#define ERR_OPEN_FILE -2
#define ERR_FILE_SIZE -3
#define ERR_UNCORRECT_COMMAND -4
#define ERR_ATTRIBUTE_BIT -5
#define ERR_WRONG_FLAG -6

int memory[100];
int reg_flags;

int sc_memoryInit();
int sc_memorySet(int, int);
int sc_memoryGet(int, int*);
int sc_memorySave(char*);
int sc_memoryLoad(char*);
int sc_regInit();
int sc_regSet(int, int);
int sc_regGet(int, int*);
int sc_commandEncode(int, int, int*);
int sc_commandDecode(int, int*, int*);

#endif // MYTERM_H
