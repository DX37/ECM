#include "../mysc/mysc.h"
#include "../myterm/myterm.h"
#include "../mybc/mybc.h"
#include "../myrk/myrk.h"
#include <stdlib.h>
#include <fcntl.h>
#include <signal.h>
#include <sys/time.h>

int ic = 0, ac = 0, cursor = 0, bigchars[256];
keys key;

int flag_state(int flag)
{
    return bit_get(reg_flags, flag);
}

void color_reset()
{
    mt_setbgcolor(Default);
    mt_setfgcolor(Default);
}

void update_memory()
{
    int cell, atr, command, operand;
    color_reset();
    bc_box(1, 1, 61, 13);
    mt_gotoXY(3, 0, " Memory \n");

    for (int i = 0; i < 100; ++i)
    {
        if (i % 10 == 0)
            mt_gotoXY(2, i/10+2, "");
        if (i == cursor)
        {
            mt_setbgcolor(green);
            mt_setfgcolor(black);
        }
        sc_memoryGet(i, &cell);

        atr = bit_get(cell, 14);
        cell &= 0x3FFF;
        command = (cell >> 7) & 0x3F;
        operand = cell & 0x3F;

        if (atr == 0)
            printf("+%02X%02X", command, operand);
        else
            printf(" %04X", cell);

        color_reset();
        printf(" ");
    }
}

void update_bigchars()
{
    int cell, atr, command, operand;
    char str[6];

    sc_memoryGet(cursor, &cell);
    atr = bit_get(cell, 14);
    cell &= 0x3FFF;
    command = (cell >> 7) & 0x3F;
    operand = cell & 0x3F;

    if (atr == 0)
        sprintf(str, "+%02d%02d", command, operand);
    else
        sprintf(str, " %04d", cell);

    color_reset();
    bc_box(0, 13, 40, 23);
    for (int i = 0; str[i] != '\0'; ++i)
        bc_printbigchar(bigchars + (str[i] * 2), i*8+2, 14, Default, Default);
    printf("\n");
}

void update_ic()
{
    char str[4];
    sprintf(str, "%03d", ic);

    color_reset();
    bc_box(64, 4, 84, 7);
    mt_gotoXY(65, 4, " instructionCounter ");
    mt_gotoXY(73, 5, str);
}

void update_operation()
{
    int cell, atr, command, operand;
    char str[7];

    sc_memoryGet(cursor, &cell);
    atr = bit_get(cell, 14);
    cell &= 0x3FFF;
    command = (cell >> 7) & 0x3F;
    operand = cell & 0x3F;

    if (atr == 0)
        sprintf(str, "+%02d:%02d", command, operand);
    else
        sprintf(str, " %02d:%02d", command/2, operand);

    color_reset();
    bc_box(64, 7, 84, 10);
    mt_gotoXY(69, 7, " Operation ");
    mt_gotoXY(71, 8, str);
}

void update_accumulator()
{
    char str[5];
    ac &= 0x3FFF;
    sprintf(str, "%04X", ac);

    color_reset();
    bc_box(64, 1, 84, 4);
    mt_gotoXY(68, 1, " accumulator ");
    mt_gotoXY(72, 2, str);
}

void update_flags()
{
    color_reset();
    bc_box(64, 10, 84, 13);
    mt_gotoXY(71, 10, " Flags ");

    if (flag_state(FLAG_OVERFLOW) == 0)
        mt_setfgcolor(red);
    else
        mt_setfgcolor(green);
    mt_gotoXY(70, 11, "O");

    if (flag_state(FLAG_ZERO_DIV) == 0)
        mt_setfgcolor(red);
    else
        mt_setfgcolor(green);
    mt_gotoXY(72, 11, "Z");

    if (flag_state(FLAG_OUT_MEM) == 0)
        mt_setfgcolor(red);
    else
        mt_setfgcolor(green);
    mt_gotoXY(74, 11, "M");

    if (flag_state(FLAG_INTER_IGNORE) == 0)
        mt_setfgcolor(red);
    else
        mt_setfgcolor(green);
    mt_gotoXY(76, 11, "I");

    if (flag_state(FLAG_WRONG_COM) == 0)
        mt_setfgcolor(red);
    else
        mt_setfgcolor(green);
    mt_gotoXY(78, 11, "C");

    mt_setbgcolor(Default);
    mt_setfgcolor(Default);
}

void show_keys()
{
    color_reset();
    bc_box(43, 13, 84, 23);
    mt_gotoXY(44, 13, " Keys: ");
    mt_gotoXY(44, 14, "l  - load");
    mt_gotoXY(44, 15, "s  - save");
    mt_gotoXY(44, 16, "r  - run");
    mt_gotoXY(44, 17, "t  - step");
    mt_gotoXY(44, 18, "i  - reset");
    mt_gotoXY(44, 19, "F5 - accumulator");
    mt_gotoXY(44, 20, "F6 - instructionCounter");
    mt_gotoXY(44, 21, "q  - quit");
    printf("\n\n\n");
}

void update()
{
	mt_clrscr();

    update_accumulator();

    update_ic();

    update_operation();

    update_flags();

    update_memory();

    update_bigchars();

    show_keys();
}

void ic_reset()
{
    sc_regSet(FLAG_INTER_IGNORE, 1);
    signal(SIGINT, SIG_DFL);
    alarm(0);
    ic = 0;
    cursor = ic;
    update(cursor);
}

int ALU(int command, int operand)
{
    switch (command)
    {
        case 30:    //ADD
            ac += memory[operand] & 0x3FFF;
            break;
        case 31:    //SUB
            ac -= memory[operand] & 0x3FFF;
            break;
        case 32:    //DIVIDE
            if (memory[operand] == 0)
            {
                sc_regSet(FLAG_ZERO_DIV, 1);
                ic_reset();
                return -1;
            }
            ac /= memory[operand] & 0x3FFF;
            break;
        case 33:    //MUL
            ac *= memory[operand] & 0x3FFF;
            break;
    }

    return 0;
}

int CU()
{
    int cell, command, operand, num;
    char buf[10];
    sc_memoryGet(ic, &cell);
    if (sc_commandDecode(cell, &command, &operand) == ERR_UNCORRECT_COMMAND)
    {
        sc_regSet(FLAG_WRONG_COM, 1);
        ic_reset();
        return -1;
    }

    switch (command)
    {
        case 10:    //READ
            rk_mytermregime(1, 0, 0, 0, 0);
            printf("READ: ");
            scanf("%s", buf);
            num = strtol(buf, NULL, 10);
            if (num > 0x3FFF)
            {
                sc_regSet(FLAG_OVERFLOW, 1);
                ic_reset();
                return -1;
            }
            num = (1 << 14) | num;
            sc_memorySet(operand, num);
            ++ic;
            break;
        case 11:    //WRITE
            rk_mytermregime(0, 0, 1, 1, 1);
            printf("WRITE: %X in %d cell\n", memory[operand] & 0x3FFF, operand);
            system("sleep 2");
            //scanf("\n");
            rk_mytermregime(1, 0, 0, 0, 0);
            ++ic;
            break;

        case 20:    //LOAD
            sc_memoryGet(operand, &ac);
            ++ic;
            break;
        case 21:    //STORE
            sc_memorySet(operand, ac);
            memory[operand] = (1 << 14) | memory[operand];
            ++ic;
            break;

        case 30:    //ADD
            ALU(command, operand);
            ++ic;
            break;
        case 31:    //SUB
            ALU(command, operand);
            ++ic;
            break;
        case 32:    //DIVIDE
            ALU(command, operand);
            ++ic;
            break;
        case 33:    //MUL
            ALU(command, operand);
            ++ic;
            break;

        case 40:    //JUMP
            ic = operand;
            break;
        case 41:    //JNEG
            if (ac < 0)
                ic = operand;
            else
                ++ic;
            break;
        case 42:    //JZ
            if (ac == 0)
                ic = operand;
            else
                ++ic;
            break;
        case 43:    //HALT
            ic_reset();
            break;
        default:
            ++ic;
            break;
    }

    cursor = ic;

    return 0;
}

void ic_set()
{
    if (flag_state(FLAG_INTER_IGNORE) == 1)
        return;
    CU();
    update(cursor);
}

int main()
{
    int fd, cnt, cell, num;
    int command, operand;
    char str[5], filename[256];

    struct itimerval nval, oval;

    nval.it_interval.tv_sec = 1;
    nval.it_interval.tv_usec = 0;
    nval.it_value.tv_sec = 1;
    nval.it_value.tv_usec = 0;

    signal(SIGALRM, ic_set);

    sc_memoryInit();
    sc_regInit();

    if ((fd = open("bigchars", O_CREAT | O_RDWR )) == -1)
    {
        perror("Open error\n");
        return -1;
    }

    bc_bigcharread(fd, bigchars, 128, &cnt);
    close(fd);

    do
    {
        if (flag_state(FLAG_INTER_IGNORE) == 1)
        {
            update(cursor);
            rk_readkey(&key);

            if (key == s)
            {
                rk_mytermregime(1, 0, 0, 0, 0);
                printf("Enter filename: ");
                scanf("%s", filename);
                sprintf(filename, "%s.scs", filename);

                if (sc_memorySave(filename) == ERR_OPEN_FILE)
                {
                    printf("Error: Can't open file to write\n");
                    rk_readkey(&key);
                }
                else if (sc_memorySave(filename) == ERR_FILE_SIZE)
                {
                    printf("Error: Can't save memory to file\n");
                    rk_readkey(&key);
                }

                rk_mytermregime(0, 0, 1, 1, 1);

                continue;
            }

            else if (key == l)
            {
                rk_mytermregime(1, 0, 0, 0, 0);
                printf("Enter filename: ");
                scanf("%s", filename);
                sprintf(filename, "%s.scs", filename);

                if (sc_memoryLoad(filename) == ERR_OPEN_FILE)
                {
                    printf("Error: Can't find file to load\n");
                    rk_readkey(&key);
                }
                else if (sc_memoryLoad(filename) == ERR_FILE_SIZE)
                {
                    printf("Error: Can't load memory from file\n");
                    rk_readkey(&key);
                }

                rk_mytermregime(0, 0, 1, 1, 1);

                continue;
            }

            else if (key == r)
            {
                sc_regSet(FLAG_INTER_IGNORE, 0);
                signal(SIGINT, ic_reset);
                setitimer(ITIMER_REAL, &nval, &oval);
                cursor = ic;
                update(cursor);
            }

            else if (key == t)
            {
                CU();
                update(cursor);
            }

            else if (key == i)
            {
                sc_memoryInit();
                sc_regInit();
                ac = 0;
                ic = 0;
                cursor = 0;
            }

            else if (key == enter)
            {
                rk_mytermregime(1, 0, 0, 0, 0);
                printf("Cell: ");
                scanf("%s", str);
                num = strtol(str, NULL, 16);
                if (strncmp(str, "+", 1) == 0)
                {
                    command = (num >> 8) & 0x7F;
                    operand = num & 0x7F;
                    cell = (command << 7) | operand;
                }
                else
                    cell = (1 << 14) | num;
                sc_memorySet(cursor, cell);
                rk_mytermregime(0, 0, 1, 1, 1);
            }

            else if (key == F5)
            {
                rk_mytermregime(1, 0, 0, 0, 0);
                printf("accumulator: ");
                scanf("%x", &ac);
                rk_mytermregime(0, 0, 1, 1, 1);
            }

            else if (key == F6)
            {
                rk_mytermregime(1, 0, 0, 0, 0);
                printf("instructionCounter: ");
                scanf("%d", &ic);
                rk_mytermregime(0, 0, 1, 1, 1);
                cursor = ic;
            }

            else if (key == left && cursor > 0)
                cursor -= 1;

            else if (key == right && cursor < 99)
                cursor += 1;

            else if (key == up && cursor > 9)
                cursor -= 10;

            else if (key == down && cursor < 90)
                cursor += 10;
        }

        if (ic >= 100)
            ic_reset();

    } while (key != q);

	mt_clrscr();

    return 0;
}
